<?php
// override the parent theme's the_excerpt() for post-types of cpl_agenda
// if the excerpt is not manually filled on right-hand sidebar of the post
// (stored as $post->post_excerpt),
// use the excerpt below

function set_the_custom_cpl_excerpt() {
	global $post;

	//
	if ( isset( $post ) && 'cpl_agenda' === $post->post_type ) {
		if ( empty( $post->post_excerpt ) ) {
				return 'The Cleveland Public Library Board of Trustees will hold a ' .
				get_field( 'cpl_agenda_board_type_field' ) . ' meeting on ' .
				get_field( 'cpl_agenda_date_picker' ) . ' at 12 noon on 10th floor ' .
				'in the Board Room at the Louis Stokes Wing, Main Library. Board Meetings are open to the public.';
		} else {
			return $post->post_excerpt;
		}
	}
}

add_filter( 'get_the_excerpt', 'set_the_custom_cpl_excerpt', 3 );

