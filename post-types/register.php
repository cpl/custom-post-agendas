<?php
namespace cpl\custom_post_agendas;
function cp_register_agenda() {
	$labels = array(
		'name'                   => __( 'Agendas', 'CPDOMAIN' ),
		'singular_name'          => __( 'Agenda', 'CPDOMAIN' ),
		'all_items'              => __( 'All Agendas', 'CPDOMAIN' ),
		'attributes'             => __( 'Attributes', 'CPDOMAIN' ),
		'add_new'                => __( 'Add new agenda', 'CPDOMAIN' ),
		'edit_item'              => __( 'Edit agenda', 'CPDOMAIN' ),
		'view_item'              => __( 'View agenda', 'CPDOMAIN' ),
		'view_items'             => __( 'View agendas', 'CPDOMAIN' ),
		'search_items'           => __( 'Search past agendas', 'CPDOMAIN' ),
		'item_reverted_to_draft' => __( 'Agenda is now a draft', 'CPDOMAIN' ),
		'item_updated'           => __( 'Agenda updates saved', 'CPDOMAIN' ),
		'item_published'         => __( 'Agenda is published to the public', 'CPDOMAIN' ),
		'archives'               => __( 'Past Agendas', 'CPDOMAIN' ),
		'attributes'             => __( 'Agenda Attributes', 'CPDOMAIN' ),
	);

	$args = array(
		'labels'              => $labels,
		'public'              => true,
		'has_archive'         => false,
		'supports'            => array( 'editor', 'title', 'custom-fields', 'revisions', 'excerpt', 'thumbnail' ),
		'delete_with_user'    => false,
		'exclude_from_search' => true,
		'public'              => true,
		'show_in_rest'        => true,
		'rewrite'             => array( 'slug' => 'board-agendas' ),
		'template'            => array(
			array(
				'core/paragraph',
				array(
					'align'   => 'center',
					'content' => 'Please be aware that all meetings of the Board of Library Trustees are open to in-person attendance by the public. Out of an abundance of care, all guests are also welcome to join this month’s board meeting online via Youtube.',
				),
			),
			array(
				'core/paragraph',
				array(
					'align'   => 'center',
					'content' => '12:00 – Noon',
				),
			),
			array(
				'core/buttons',
				array(
					'layout' => array(
						'type'           => 'flex',
						'justifyContent' => 'center',
					),
				),
				array(
					array(
						'core/button',
						array(
							'text'      => 'Watch on Youtube',
							'url'       => 'https://www.youtube.com/@ClevelandPublicLib/streams',
							'className' => 'is-style-cpl-button--action',
						),
					),
				),
			),
			array(
				'core/paragraph',
				array(
					'align'   => 'center',
					'content' => 'If you cannot attend the meeting, please use the "SUBMIT A COMMENT TO THE BOARD" link to send a public comment to the Board of Trustees. Your comments will be read at a designated time during the board meeting. ',
				),
			),
			array(
				'core/buttons',
				array(
					'layout' => array(
						'type'           => 'flex',
						'justifyContent' => 'center',
					),
				),
				array(
					array(
						'core/button',
						array(
							'text'      => 'Submit a comment to the board',
							'url'       => 'https://cpl.org/aboutthelibrary/comments-to-the-board-of-trustees/',
							'className' => 'is-style-cpl-button--action',
						),
					),
				),
			),
			array(
				'core/list',
				array(
					'ordered'   => true,
					'className' => 'cpl-list-board--agenda',
					'lock'      => array(
						'move'   => true,
						'remove' => true,
					),
				),
			),
		),
		'map_meta_cap'        => true,
		'capabilities'        => array(
			//describes as metacapabilities
			// https://codex.wordpress.org/Function_Reference/register_post_type
			'create_posts'           => 'create_cpl_agendas',
			'read_post'              => 'read_cpl_agenda',
			'read_private_posts'     => 'read_private_cpl_agendas',
			'edit_post'              => 'edit_cpl_agenda',
			'edit_posts'             => 'edit_cpl_agendas',
			'edit_others_posts'      => 'edit_others_cpl_agendas',
			'edit_published_posts'   => 'edit_published_cpl_agendas',
			'edit_private_posts'     => 'edit_private_cpl_agendas',
			'delete_post'            => 'delete_cpl_agenda',
			'delete_posts'           => 'delete_cpl_agendas',
			'delete_others_posts'    => 'delete_other_cpl_agendas',
			'delete_published_posts' => 'delete_published_cpl_agendas',
			'delete_private_posts'   => 'delete_private_cpl_agendas',
			'publish_posts'          => 'publish_cpl_agendas',

		),
	);
		register_post_type( 'cpl_agenda', $args );
}

// determine which columns at edit.php?post_type=cpl_agenda display
// seems a little hacky in that I just generated
// t3h_agenda_date; you can also rename column_name as well
function modify_cpl_agendas_post_admin_columns( $columns ) {
	unset( $columns['title'] );
	unset( $columns['date'] );
	unset( $columns['last-modified'] );
	return array_merge(
		$columns,
		array(
			't3h_agenda_date'  => __( 'Agenda Date' ),
			't3h_meeting_type' => __( 'Meeting Type' ),
			'author'           => __( 'Agenda Author' ),
		)
	);
}
add_filter( 'manage_cpl_agenda_posts_columns', __NAMESPACE__ . '\modify_cpl_agendas_post_admin_columns' );

function add_column_to_cpl_agenda_admin_screen( $column_name ) {
	global $post;
	if ( $column_name == 't3h_agenda_date' ) {
		the_field( 'cpl_agenda_date_picker', $post->ID );
	}
	if ( $column_name == 't3h_meeting_type' ) {
		the_field( 'cpl_agenda_board_type_field', $post->ID );
	}
}

add_action( 'manage_cpl_agenda_posts_custom_column', __NAMESPACE__ . '\add_column_to_cpl_agenda_admin_screen', 10, 1 );
