<?php
namespace cpl\custom_post_agendas;
// changes the upload path for media when post_type of cpl_agenda is open 
// within Gutenberg/block-editor
// upload path is changed to /uploads/board/

function cpl_agenda_filter_upload_path( $args ) {

	// Get the current post_id
	$id = ( isset( $_POST['post_id'] ) ? $_POST['post_id'] : '' );

	//
	if ( \get_post_type( $id ) === 'cpl_agenda' ) {
		$newdir = '/' . 'board';

		$args['path']   = str_replace( $args['subdir'], '', $args['path'] ); //remove default subdir
		$args['url']    = str_replace( $args['subdir'], '', $args['url'] );
		$args['subdir'] = $newdir;
		$args['path']  .= $newdir;
		$args['url']   .= $newdir;
	}
	return $args;
}
add_filter( 'upload_dir', __NAMESPACE__ . '\cpl_agenda_filter_upload_path' );
